if FORMAT ~= 'latex' then
  return {}
end

return {
    {
        Str = function (elem)
            pattern = ":([%l%+%d_]+):"
            s = elem.text
            i, _ = string.find(elem.text, pattern)
            if i == nil then
                return nil
            end
            s, _ = string.gsub(s, pattern, function (n)
                n1, _ = string.gsub(n, "_", "-")
                return "\\emoji{" .. n1 .. "}"
            end)
            return pandoc.RawInline("latex", s)
        end,
    }
}
