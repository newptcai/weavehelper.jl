function Str(elem)
    local original_text = elem.text
    -- Replace the Unicode character ☒ with a Unicode character for an empty checkbox (☐, U+2610)
    local modified_text = string.gsub(original_text, "☒", "☐")
    
    return pandoc.Str(modified_text)
end

return {
    {Str = Str}
}
