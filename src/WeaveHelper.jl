module WeaveHelper

using Weave, RelocatableFolders, LaTeXStrings, Latexify, Random, SymPy

export md,
    html,
    pdf,
    srcfile,
    short_answer,
    long_answer,
    quiz_pagebreak,
    quiz_vfill,
    print_in_quiz,
    print_in_solution,
    print_empty_box,
    print_checked,
    print_end,
    print_space_on_next_page,
    seed!,
    set_to_solution,
    unset_to_solution,
    process_latex,
    join_latex

## Directories
const PKG_DIR = normpath(@__DIR__, "..")
const ASSETS_DIR = @path joinpath(PKG_DIR, "assets")
const TEX_HEADER = normpath(ASSETS_DIR, "header.tex")
const EMOJI_FILTER = normpath(ASSETS_DIR, "emoji.lua")
const CHECKBOX_FILTER = normpath(ASSETS_DIR, "checkbox.lua")

## Global Variables
is_solution = false

"""
    fix()

Work around the bug of hanging at GR plots.
"""
function fix()
    ENV["GKSwstype"] = "nul"
end

"""
    srcfile()

Find the first file in the current directory that ends with ".jmd".

# Returns
- `String`: The name of the first `.jmd` file found, or an empty string if none is found.
"""
function srcfile()
    files = readdir()
    src = ""
    for f in files
        _, fext = splitext(f)
        if fext == ".jmd"
            src = f
            break
        end
    end
    src
end

"""
    md(src::String)

Compile the given `src` file into a markdown file.

# Arguments
- `src::String`: The path to the `.jmd` file to compile.

# Returns
- `Nothing`
"""
function md(src)
    fix()
    weave(src, out_path = :pwd, doctype = "pandoc")
end

"""
    md()

Compile the default `.jmd` file into a markdown file.

# Returns
- `Nothing`
"""
md() = md(srcfile())

"""
    html(src::String)

Compile the given `src` file into an HTML file.

# Arguments
- `src::String`: The path to the `.jmd` file to compile.

# Returns
- `Nothing`
"""
function html(src)
    fix()
    pandoc_options = ["--from=markdown+emoji", "-s", "--mathjax"]
    weave(src, out_path = :pwd, doctype = "pandoc2html", pandoc_options = pandoc_options)
end

"""
    html()

Compile the default `.jmd` file into an HTML file.

# Returns
- `Nothing`
"""
html() = html(srcfile())

"""
    pdf(src::String, header="$TEX_HEADER"; pandoc_options::Vector{String}=String[], kwargs...)

Compile the given `src` file into a PDF file, with an optional LaTeX header.

# Arguments
- `src::String`: The path to the `.jmd` file to compile.
- `header::String`: The path to the LaTeX header file (default is `$TEX_HEADER`).
- `pandoc_options::Vector{String}`: Additional pandoc options to customize the PDF output.
- `kwargs...`: Additional keyword arguments passed to `weave`.

# Returns
- `Nothing`
"""
function pdf(src, header="$TEX_HEADER"; pandoc_options=String[], kwargs...)
    fix()
    default_pandoc_options = [
        "--pdf-engine=lualatex",
        "--include-in-header=$header",
        "--lua-filter=$EMOJI_FILTER"
    ]

    if !is_solution
        append!(default_pandoc_options, ["--lua-filter=$CHECKBOX_FILTER"])
    end

    merged_pandoc_options = vcat(default_pandoc_options, pandoc_options)

    if is_solution
        out_path = replace(src, ".jmd" => "-solution.pdf")
    else
        out_path = replace(src, ".jmd" => ".pdf")
    end

    weave(
        src,
        out_path=out_path,
        doctype="pandoc2pdf",
        pandoc_options=merged_pandoc_options;
        kwargs...)
end

"""
    pdf(; kwargs...)

Compile the default `.jmd` file into a PDF file.

# Arguments
- `kwargs...`: Additional keyword arguments passed to `pdf(src; kwargs...)`.

# Returns
- `Nothing`
"""
pdf(; kwargs...) = pdf(srcfile(); kwargs...)

"""
    all()

Compile the default `.jmd` file into all possible output formats.

# Returns
- `Nothing`
"""
all() = all(srcfile())

"""
    all(src::String)

Compile the given `src` file into all possible output formats.

# Arguments
- `src::String`: The path to the `.jmd` file to compile.

# Returns
- `Nothing`
"""
function all(src)
    md(src)
    html(src)
    pdf(src)
end

# Seeding
const MYSEED = 1234

"""
    seed!()

Seed the random number generator with `MYSEED`.

# Returns
- `Nothing`
"""
function seed!()
    Random.seed!(MYSEED)
end

"""
    seed!(seed::Int)

Seed the random number generator with `MYSEED + seed`.

# Arguments
- `seed::Int`: The seed to be added to `MYSEED`.

# Returns
- `Nothing`
"""
function seed!(seed)
    seed!(MYSEED + seed)
end

"""
    process_latex(latex::LaTeXString)

Process a LaTeX string and remove unnecessary \$ at the beginning and end.

# Arguments
- `latex::LaTeXString`: The LaTeX string to process.

# Returns
- `LaTeXString`: The processed LaTeX string.
"""
function process_latex(latex::LaTeXString)
    if latex[1] == '$'
        return latex[2:end-1]
    else
        return latex
    end
end

"""
    process_latex(num::Number)

Convert a number to a LaTeX string.

# Arguments
- `num::Number`: The number to convert.

# Returns
- `String`: The LaTeX string representation of the number.
"""
function process_latex(num::Number)
    return string(sympy.latex(Sym(num)))
end

"""
    process_latex(latex::String)

Return the input LaTeX string unchanged.

# Arguments
- `latex::String`: The LaTeX string.

# Returns
- `String`: The same LaTeX string.
"""
function process_latex(latex::String)
    return latex
end

"""
    process_latex(latex::Sym)

Convert a SymPy symbol to LaTeX.

# Arguments
- `latex::Sym`: The SymPy symbol to convert.

# Returns
- `String`: The LaTeX string representation of the symbol.
"""
function process_latex(latex::Sym)
    return string(sympy.latex(latex, order = "old"))
end

"""
    join_latex(parts...)

Join multiple LaTeX strings and other objects.

# Arguments
- `parts...`: A variable number of LaTeX strings or other objects to be joined.

# Returns
- `String`: The joined LaTeX string.
"""
function join_latex(parts...)
    joined_str = join(process_latex(p) for p in parts)
    return joined_str
end

"""
    fill_box(content)

Generate a LaTeX box with the given content.

# Arguments
- `content::LaTeXString`: The content to put inside the box.

# Returns
- `LaTeXString`: The LaTeX code for the box.
"""
function fill_box(content)
    return process_latex(L"\vcenter{\hbox{\boxed{%$(content)}}}")
end

"""
    fill_empty_box(width=8, height=1.5)

Generate a LaTeX empty box with the given width and height.

# Arguments
- `width::Float64`: The width of the empty box (default is 8).
- `height::Float64`: The height of the empty box (default is 1.5).

# Returns
- `LaTeXString`: The LaTeX code for the empty box.
"""
function fill_empty_box(width = 8, height = 1.5)
    return process_latex(
        fill_box(L"\rule{0pt}{%$(height)cm}\phantom{\hspace{%$(width)cm}}"),
    )
end

"""
    print_empty_box(weight = 8, height = 1.5)

Print an empty box with the specified width and height.

# Arguments
- `weight::Float64`: The width of the box (default is 8).
- `height::Float64`: The height of the box (default is 1.5).

# Returns
- `Nothing`
"""
function print_empty_box(weight = 8, height = 1.5)
    print(process_latex(fill_empty_box(weight, height)))
end

"""
    set_to_solution()

Set the mode to generate solutions for a quiz.

# Returns
- `Bool`: `true` if the solution mode is set, otherwise `false`.
"""
function set_to_solution()
    global is_solution = true
    return is_solution
end

"""
    set_to_solution(is_sol::Bool)

Set or unset the solution mode for a quiz.

# Arguments
- `is_sol::Bool`: A boolean flag indicating whether to set solution mode (`true`) or not (`false`).

# Returns
- `Bool`: The updated solution mode state.
"""
function set_to_solution(is_sol)
    global is_solution = is_sol
    return is_solution
end

"""
    unset_to_solution()

Unset the solution mode to generate a quiz without solutions.

# Returns
- `Bool`: `false` indicating that solution mode is not set.
"""
function unset_to_solution()
    global is_solution = false
    return is_solution
end

"""
    short_answer(prompt::String, answer::String; is_solution=is_solution, append="", connector="=", width=8, height=1.25, add_heading=true)

Print space or fill in a short answer.

# Arguments
- `prompt::String`: The prompt or question to display.
- `answer::String`: The correct answer to display (or space if in quiz mode).
- `is_solution::Bool`: Whether the solution is being printed (default is `is_solution`).
- `append::String`: Text to append after the answer (default is an empty string).
- `connector::String`: The connector symbol between the prompt and the answer (default is "=").
- `width::Float64`: The width of the box for the answer (default is 8).
- `height::Float64`: The height of the box for the answer (default is 1.25).
- `add_heading::Bool`: Whether to add the "Answer:" heading (default is `true`).

# Returns
- `Nothing`
"""
function short_answer(
    prompt,
    answer;
    is_solution = is_solution,
    append = "",
    connector = "=",
    width = 8,
    height = 1.25,
    add_heading = true,
)
    answer = process_latex(answer)
    prompt = process_latex(prompt)
    conn = process_latex(connector)
    if append != ""
        append = process_latex(append)
    end
    heading = add_heading ? "Answer:" : ""
    if is_solution
        fill_in = fill_box(answer)
    else
        fill_in = fill_empty_box(width, height)
    end

    print(process_latex(L"""
    %$heading
    \begin{equation*}
        %$prompt %$conn %$fill_in %$append
    \end{equation*}
    """))
end

"""
    short_answer(answer::String; kwargs...)

Print space or fill in a short answer without any prefix.

# Arguments
- `answer::String`: The answer (or space if in quiz mode).
- `kwargs...`: Additional keyword arguments passed to `short_answer(prompt, answer; kwargs...)`.

# Returns
- `Nothing`
"""
function short_answer(answer; kwargs...)
    short_answer(L"", answer; connector = "", kwargs...)
end

"""
    long_answer(answer::String; is_solution=is_solution, height=5, vfill=false, pagebreak=false, page_after=0)

Print space or fill in a long answer.

# Arguments
- `answer::String`: The answer (or space if in quiz mode).
- `is_solution::Bool`: Whether the solution is being printed (default is `is_solution`).
- `height::Float64`: The height of the answer box (default is 5).
- `vfill::Bool`: Whether to use vertical fill space (default is `false`).
- `pagebreak::Bool`: Whether to insert a page break after the answer (default is `false`).
- `page_after::Int`: The number of empty pages to insert after the answer (default is 0).

# Returns
- `Nothing`
"""
function long_answer(
    answer;
    is_solution = is_solution,
    height = 5,
    vfill = false,
    pagebreak = false,
    page_after = 0,
)
    if is_solution
        fill_in = process_latex(answer)
    else
        if vfill
            fill_in = process_latex(L"""
            \null{}
            \vfill{}
            """)
        else
            fill_in = process_latex(L"""
            \vspace{%$(height)cm}
            """)
        end
    end

    if page_after > 0
        print("""
              Answer (There is more space on the next page):

              """)
    else
        print("""
              Answer:

              """)
    end

    print(process_latex(L"""
    %$fill_in
    """))

    if pagebreak
        quiz_pagebreak()
    end

    for _ = 1:page_after
        print_empty_page()
    end
end

"""
    print_empty_page()

Print an empty page if not in solution mode.

# Returns
- `Nothing`
"""
function print_empty_page()
    if !is_solution
        template = L"""
        \pagebreak{}
        \begin{center}
            --- Use this page to answer the preceding question if further space is required. ---
        \end{center}
        \vfill{}
        """
        print_in_quiz(template)
    end
end

"""
    print_in_quiz(text::String)

Print the given text if `is_solution` is false.

# Arguments
- `text::String`: The text to print.

# Returns
- `Nothing`
"""
function print_in_quiz(text)
    if !is_solution
        print(process_latex(text))
    end
end

"""
    print_in_solution(text::String)

Print the given text if `is_solution` is true.

# Arguments
- `text::String`: The text to print.

# Returns
- `Nothing`
"""
function print_in_solution(text)
    if is_solution
        print(process_latex(text))
    end
end

"""
    print_checked()

Print a checked box in solution mode and an empty box in quiz mode.

# Returns
- `Nothing`
"""
function print_checked()
    print_in_quiz("[ ]")
    print_in_solution("[X]")
end

"""
    quiz_pagebreak()

Insert a page break in the quiz.

# Returns
- `Nothing`
"""
function quiz_pagebreak()
    print_in_quiz(L"\pagebreak{}")
end

"""
    quiz_vfill()

Print vertical space for students to write solutions in the quiz.

# Returns
- `Nothing`
"""
function quiz_vfill()
    print_in_quiz(L"\null\vfill")
end

"""
    print_end()

Print the end of the quiz with random emojis.

# Returns
- `Nothing`
"""
function print_end()
    function generate_random_emoji_line(emoji_list::Vector{String}, count::Int)
        emoji_line = ""
        for _ = 1:count
            emoji = rand(emoji_list)
            emoji_line *= "\\emoji{" * emoji * "} "
        end
        return emoji_line
    end

    emojis = [
        "carrot",
        "star",
        "heart",
        "rabbit-face",
        "tulip",
        "sparkles",
        "sunflower",
        "butterfly",
    ]

    top_bottom_line = generate_random_emoji_line(emojis, 10)

    the_end = L"""
    \begin{center}
    %$top_bottom_line
    \hfil
    \textbf{The End of The Quiz}
    \hfil
    %$top_bottom_line
    \end{center}
    """

    print(process_latex(the_end))
end

"""
    print_space_on_next_page()

Print space on the next page.

# Returns
- `Nothing`
"""
function print_space_on_next_page()
    text = L"""
    \vfill{}
    \begin{center}
        (More space on the next page.)
    \end{center}
    \pagebreak{}
    \null{}
    \vfill{}
    """
    print_in_quiz(text)
end

# Set default LaTeX equation environment for Latexify
set_default(env = :raw)

end
