using WeaveHelper
using Test

@testset "WeaveHelper.jl" begin
    # Write your tests here.

    # Copy files to a temporary test directory
    testfile = "test.jmd"
    tmpdir = mktempdir(cleanup = false)
    tmpfile = joinpath(tmpdir, testfile)
    cp(testfile, tmpfile)
    cd(tmpdir)

    @testset "run" begin
        @test pdf() != ""
        @test html() != ""
        @test md() != ""
    end
end
